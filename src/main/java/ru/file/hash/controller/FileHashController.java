package ru.file.hash.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.file.hash.model.FileHash;
import ru.file.hash.service.FileServiceImpl;

import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/file_hashes")
public class FileHashController {

    @Autowired
    private FileServiceImpl service;

    @PostMapping
    public void uploadFile(@RequestParam("file") MultipartFile file) throws URISyntaxException {
        service.save(file);
    }

    @GetMapping("/{hash}")
    public List<FileHash> find(@PathVariable String hash){
        return service.findByHash(hash);
    }

    @DeleteMapping("/{hash}")
    public List<FileHash> delete(@PathVariable String hash){
        return service.deleteByHash(hash);
    }
}
